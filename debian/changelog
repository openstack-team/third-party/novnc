novnc (1:1.3.0-3) unstable; urgency=medium

  * Add python3-setuptools as build-depends (Closes: #1080685).

 -- Thomas Goirand <zigo@debian.org>  Tue, 24 Sep 2024 09:53:56 +0200

novnc (1:1.3.0-2) unstable; urgency=medium

  * Added extend-diff-ignore, cleans better (Closes: #1046241).

 -- Thomas Goirand <zigo@debian.org>  Thu, 17 Aug 2023 17:32:00 +0200

novnc (1:1.3.0-1) unstable; urgency=medium

  * Added changes from Ricardo Ribalda Delgado. Thanks a lot to him for his
    contribution. Here's the changes (Closes: #927408):
    - New upstream release.
    - Use gitmode in watch file.
    - Add salsa-ci.yml.
    - Fix perms of genkeysymdef.js and use_require.js.
    - Fix debian/novnc.install for this new release.
    - Fix d/copyright for this release.
    - Switch to debhelper-compat 11.
    - Use python3-all:any instead of just python3-all for b-d.
    - Standards-Version: 4.6.0.
    - Add Rules-Requires-Root: no.
    - add nodejs as depends.
    - Removed fix-for-python3.patch (there's no .py files upstream anymore).
    - Rebased use-debian-folder-node-getopt.patch.
  * vnc_auto.html links to vnc.html, which is the full version, not the light
    one.

 -- Thomas Goirand <zigo@debian.org>  Fri, 04 Nov 2022 13:17:35 +0100

novnc (1:1.0.0-5) unstable; urgency=medium

  * Add net-tools as depends, needed for launch.sh (Closes: #932611).

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Dec 2021 13:54:20 +0100

novnc (1:1.0.0-4) unstable; urgency=medium

  * Do not create symlink to swfobject.js (Closes: #924153).

 -- Thomas Goirand <zigo@debian.org>  Tue, 28 Dec 2021 18:49:17 +0100

novnc (1:1.0.0-3) unstable; urgency=medium

  * Fix symlink to swfobject.js (Closes: #924153)

 -- Michal Arbet <michal.arbet@ultimum.io>  Thu, 12 Mar 2020 12:31:52 +0100

novnc (1:1.0.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * Removed python 2 support (Closes: #934930).

 -- Thomas Goirand <zigo@debian.org>  Mon, 26 Aug 2019 23:42:51 +0200

novnc (1:1.0.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https in Format
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ David Rabel ]
  * d/changelog: Fix wrong entry.
  * d/copyright: Fixed. Was totally broken.
  * d/novnc.install: Don't install extra license file.
  * d/patches/ : Fix typo.
  * d/control, d/watch: Update upstream URL.

  [ Thomas Goirand ]
  * New upstream release (Closes: #847122):
    - Drop now useless patches: utils-launch.patch and
      adds_support_for_secure_attribute_on_token_cookie.patch
  * Do not override dh_clean anymore.
  * Fixed patch to package novnc as python package.
  * Rewrite debian/novnc.install and part of debian/rules to adapt to the new
    upstream release..
  * Add patch for Python 3 compat (print statements).
  * Hack po/po2js to use debian/getopt.js.
  * Update debian/copyright file to reality of 1.0.0.
  * Fixed debian/rules to use new upstream URL.
  * Add Python 3 support.
  * Document license for vendor/pako/lib/zlib/.
  * Removed runtime dependency on libjs-swfobject, which is only for old web
    browsers anyway (Closes: #908693).
  * Add compatibility symlink to vnc_auto.html so that it works with old Nova
    setup without any modification.
  * Delete /usr/share/novnc/vendor/browser-es-module-loader/.npmignore during
    the file install.
  * Fixed shebang for b64-to-binary.pl.

 -- Thomas Goirand <zigo@debian.org>  Tue, 15 May 2018 11:28:13 +0200

novnc (1:0.4+dfsg+1+20131010+gitf68af8af3d-7) unstable; urgency=medium

  [ David Rabel ]
  * Suggests instead of recommends python-nova (Closes: #867389).

  [ Thomas Goirand ]
  * Run wrap-and-sort -bast.
  * Standards-Version is now 4.1.3.
  * VCS fields now pointing to Salsa.
  * Using debhelper 11.

 -- Thomas Goirand <zigo@debian.org>  Thu, 25 Jan 2018 13:06:47 +0000

novnc (1:0.4+dfsg+1+20131010+gitf68af8af3d-6) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed homepage (https).
  * Fixed VCS URLs (https).
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/copyright: Changed source URL to https protocol
  * d/s/options: extend-diff-ignore of .gitreview
  * d/control: Using OpenStack's Gerrit as VCS URLs.

  [ Thomas Goirand ]
  * Applied patch contributed in launchpad (Closes: #837997).
  * Ran wrap-and-sort -t -a.
  * Build-depends on dh-python.
  * Removed Pre-Depends: dpkg (>= 1.15.6~).
  * Removed inactive uploaders.
  * Standards-Version: 3.9.8 (no change).

 -- Thomas Goirand <zigo@debian.org>  Sun, 09 Oct 2016 01:00:48 +0200

novnc (1:0.4+dfsg+1+20131010+gitf68af8af3d-4) unstable; urgency=high

  * Adds security patch (Closes: #778618):
    adds_support_for_secure_attribute_on_token_cookie.patch
  * Adds a source.lintian-overrides for include/logo.js. This is a logo, and
    not code source, so I believe it should be fine. Moreover, the file in
    include/logo.js.png is the source, and running "base64 include/logo.js.png"
    generates the same data, so it is my point of view that we're in the case
    of a false positive. If one difers, please provide the patch to generate
    the logo.js out of the logo.js.png file.

 -- Thomas Goirand <zigo@debian.org>  Mon, 02 Mar 2015 17:27:07 +0100

novnc (1:0.4+dfsg+1+20131010+gitf68af8af3d-3) unstable; urgency=medium

  * Drops python-nova as a direct dependency, and only puts it as a
    Recommends:.

 -- Thomas Goirand <zigo@debian.org>  Thu, 12 Feb 2015 22:52:35 +0100

novnc (1:0.4+dfsg+1+20131010+gitf68af8af3d-2) unstable; urgency=medium

  * Do not create nova user and group if they already exist.

 -- Thomas Goirand <zigo@debian.org>  Wed, 25 Dec 2013 16:47:06 +0800

novnc (1:0.4+dfsg+1+20131010+gitf68af8af3d-1) unstable; urgency=low

  * New upstream release (well, a more recent commit since upstream isn't
    doing much tagged release...).
  * Kill the wrong patch that landed in the master-dfsg branch somehow, and
    broke novnc (Closes #718889).

 -- Thomas Goirand <zigo@debian.org>  Fri, 11 Oct 2013 22:28:34 +0800

novnc (1:0.4+dfsg+1+20130425+git4973b9cc80-1) unstable; urgency=low

  * Uploading to unstable.
  * New upstream release.
  * Added dependency on oslo-config.
  * Updated debian/patches/python-disutils.patch.
  * Removed rebind and websockify from /usr/share/util, since they are
    available in the websockify package.
  * Now depends on libjs-swfobject and use that file instead of the embbeded
    version.
  * Switch novnc from arch: any to arch: all since there's no built binaries
    in the package anymore (those are in websockify).
  * Delete the debian/novnc.init, because the init script is in nova-novncproxy
    already.
  * Ran wrap-and-sort.

 -- Thomas Goirand <zigo@debian.org>  Mon, 20 May 2013 23:55:04 +0800

novnc (1:0.4+dfsg+1-7) experimental; urgency=low

  * Added missing usr/share/novnc folder (Closes: #705669), thanks to
    Denis Laxalde <denis.laxalde@logilab.fr> for reporting it.

 -- Thomas Goirand <zigo@debian.org>  Tue, 23 Apr 2013 21:53:58 +0800

novnc (1:0.4+dfsg+1-6) experimental; urgency=low

  * Removes /usr/bin/websockify and /usr/bin/rebind, and now depends on the
    websockify package which has them.

 -- Thomas Goirand <zigo@debian.org>  Wed, 27 Mar 2013 22:26:08 +0800

novnc (1:0.4+dfsg+1-5) experimental; urgency=low

  [ Mehdi Abaakouk ]
  * New upstream release
  * Change uptream URL
  * Change epoch to follow novnc version

  [ Ghe Rivero]
  * Removed depend on nova-common
  * Split config file

  [ Thomas Goirand ]
  * Added a get-vcs-source target.
  * Added a gbp.conf file.
  * Now using openstack-pkg-tools for packaging.
  * Increased compat level to 9, now the compat level and debhelper
    build-depends are matching (sic!!!).
  * Pre-Depends: dpkg (>= 1.15.6~) because of xz compression.
  * Reviewed long description.
  * Added a watch file.
  * Added a patch description for the distutil patch.
  * Added missing dependency python-nova.

 -- Thomas Goirand <zigo@debian.org>  Sat, 02 Feb 2013 16:47:45 +0000

novnc (2012.1~e3+dfsg+1-4) unstable; urgency=low

  * replace debian/rules build: target with override_dh_auto_build-arch:
    otherwise it is ignored and fails when running dpkg-buildpackage -B

 -- Loic Dachary (OuoU) <loic@debian.org>  Tue, 26 Jun 2012 21:57:28 +0200

novnc (2012.1~e3+dfsg+1-3) unstable; urgency=low

  * Add a build-depend on python-greenlet. There is no actual need for
    python-greenlet at build time. But on some architectures
    python-greenlet is not yet available and novnc should not try to build
    because the resulting package will fail to install. This workaround
    should not be necessary but the maintainer of python-greenlet has not
    resolved portability issues in months at this time and we cannot
    assume it will be resolved quickly. (Closes: #665893).

 -- Loic Dachary (OuoU) <loic@debian.org>  Tue, 26 Jun 2012 11:17:31 +0200

novnc (2012.1~e3+dfsg+1-2) unstable; urgency=low

  [ Ghe Rivero ]
  * Bumped Debian Policy Version
  * Updated copyright file

  [ Thomas Goirand ]
  * Added a debian/novnc.postinst creating nova user and group.
  * Added a debian/gbp.conf

 -- Thomas Goirand <zigo@debian.org>  Sun, 11 Mar 2012 10:31:31 -0400

novnc (2012.1~e3+dfsg-1) unstable; urgency=low

  * New upstream release

 -- Ghe Rivero <ghe.rivero@stackops.com>  Mon, 30 Jan 2012 17:09:57 +0100

novnc (0.1) maverick; urgency=low

  * First upstream release

 -- Joel Martin <github@martintribe.org>  Tue, 05 Jul 2011 01:00:00 -0600
